# Nick Busey Twitch Stream Assets

I believe in Open Source, and try to release as much as I can in Open Source format.

On my [Twitch stream](https://www.twitch.tv/nickbusey) I work exclusively on open source software,
I thought it only made sense to release what drives the stream itself as open source assets as well.

I have so far included my OBS Scene export, and my blender assets.